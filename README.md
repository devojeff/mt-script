# Скрипты для MetaTrader

Если не указано другое, код доступен как для MT4, так и для MT5.

Далее под папкой `MQL` предполагается папка `MQL4` или `MQL5` в зависимости от версии терминала.

Скрипты, для которых есть версии для MT4 и MT5, могут быть идентичными по содержанию. Приходится использовать отдельные файлы, так как MetaTrader распознаёт версию MQL исключительно по расширению файла.

## Скрипты

### ColorManager

Скрипт для управления цветовой схемой графика. Подробнее: <https://fxcoder.blogspot.com/2012/08/colormanager-script.html>.

При первом запуске будет создан файл с небольшим набором цветовых схем. Цветовые схемы расположены в файле `MQL/Files/ColorManager/colors.ini`.

## Индикаторы

### MultiStoch

Несколько индикаторов Stochastic в одном окне. Подробнее: <https://fxcoder.blogspot.com/2012/05/multistoch-indicator.html>.

### VP

Распределение сделок по ценовым уровням на заданном временном участке. Подробнее: <https://fxcoder.blogspot.com/2009/09/volume-profile-indicator.html>.

<!--

### Chart

Индикатор для отображения графика в виде свечек в дополнительном окне. Подробнее: <https://fxcoder.blogspot.com/2011/06/chart-indicator.html>. Только MT4.

### EA-Line и EA-Line0

Набор индикаторов для построения графиков по данным торговых роботов при визуальном тестировании в MetaTrader. Подробнее: <https://fxcoder.blogspot.com/2013/07/ea-line-indicator.html>.

### Index

Индекс на основе среднего геометрического. Подробнее: <https://fxcoder.blogspot.com/2011/06/index-indicator.html>.

### MA

Скользящая средняя (Moving Average) с индикацией касания цены. Подробнее: <https://fxcoder.blogspot.com/2011/06/ma-indicator.html>.

### StdScore

Расчет стандартной оценки и вывод ее в виде свечей. Подробнее: <https://fxcoder.blogspot.com/2013/07/stdscore-indicator.html>. Только MT4.
-->
