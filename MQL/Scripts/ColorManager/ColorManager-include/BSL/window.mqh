/*
Copyright 2019 FXcoder

This file is part of ColorManager.

ColorManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ColorManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ColorManager. If not, see
http://www.gnu.org/licenses/.
*/

// Класс окна Windows, 5.1880/4.1126. Better Standard Library. © FXcoder

#property strict

#include "import/user32.mqh"
#include "util/flag.mqh"


class CBWindow
{
protected:

	int handle_;

	// const
	const static uint
		GA_PARENT, GA_ROOT,
		GWL_STYLE, GWL_EXSTYLE,
		WS_MINIMIZE, WS_MAXIMIZE,
		WM_KEYDOWN, WM_COMMAND,
		SW_MAXIMIZE, SW_MINIMIZE, SW_RESTORE;


public:

	void CBWindow(): handle_(0) { }
	void CBWindow(int handle): handle_(handle) { }

	int  handle() const { return(handle_); }
	void handle(int handle) { handle_ = handle; }

	bool is_valid() const { return(handle_ != 0); }

	bool select_parent()      { handle_ = this.parent_hadle();      return(this.is_valid()); }
	bool select_root_parent() { handle_ = this.root_parent_hadle(); return(this.is_valid()); }

	int parent_hadle()      const { return(user32::GetAncestor(handle_, GA_PARENT)); }
	int root_parent_hadle() const { return(user32::GetAncestor(handle_, GA_ROOT));   }

	bool is_maximized() const { return(this.get_style_flag(WS_MAXIMIZE)); }
	bool is_minimized() const { return(this.get_style_flag(WS_MINIMIZE)); }

	void maximize() const { user32::ShowWindow(handle_, SW_MAXIMIZE); }
	void minimize() const { user32::ShowWindow(handle_, SW_MINIMIZE); }
	void restore()  const { user32::ShowWindow(handle_, SW_RESTORE);  }

	void key_down(int key) const
	{
		user32::PostMessageA(handle_, WM_KEYDOWN, key, 0);
	}

	void command(int command) const
	{
		user32::PostMessageA(handle_, WM_COMMAND, command, 0);
	}


protected:

	// service methods

	void set_style_flag(int flag) const
	{
		int style = user32::GetWindowLongA(handle_, GWL_STYLE);

		// не работать для развёрнутого окна
		if (_flag.check(style, WS_MAXIMIZE))
			return;
			
		_flag.set(style, flag);

		user32::SetWindowLongA(handle_, GWL_STYLE, style);
		user32::SetWindowPos(handle_, 0, 0, 0, 0, 0, 39);
	}

	bool get_style_flag(int flag) const
	{
		int style = user32::GetWindowLongA(handle_, GWL_STYLE);
		return(_flag.check(style, flag) != 0);
	}
};

// GetAncestor
const uint CBWindow::GA_PARENT     = 1;  // Retrieves the parent window. This does not include the owner, as it does with the GetParent function.
const uint CBWindow::GA_ROOT       = 2;  // Retrieves the root window by walking the chain of parent windows.

// Window Style
const uint CBWindow::GWL_STYLE    = -16;
const uint CBWindow::GWL_EXSTYLE  = -20;

const uint CBWindow::WS_MINIMIZE  = 0x20000000;
const uint CBWindow::WS_MAXIMIZE  = 0x01000000;

// Keys
const uint CBWindow::WM_KEYDOWN   = 0x0100;
const uint CBWindow::WM_COMMAND   = 0x0111;

// Show Window
const uint CBWindow::SW_MAXIMIZE  = 3;
const uint CBWindow::SW_MINIMIZE  = 6;
const uint CBWindow::SW_RESTORE   = 9;

