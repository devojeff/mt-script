/*
Copyright 2019 FXcoder

This file is part of ColorManager.

ColorManager is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ColorManager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with ColorManager. If not, see
http://www.gnu.org/licenses/.
*/

// Класс окна графика, 5.1880/4.1126. Better Standard Library. © FXcoder

#property strict

#include "window.mqh"

/*
Класс окна, содержащего окно графика.
Это окно содержит рамки и кнопки окна, а также окно-контейнер, в котором
расположен график.
*/
class CBChartWindow : public CBWindow
{
protected:

	// запомнить объект окна-контейнера графика для особых команд (например, обновление)
	CBWindow chart_window_;


public:

	void CBChartWindow(long chart_id = 0):
		CBWindow()
	{
		handle_ = int(::ChartGetInteger(chart_id, CHART_WINDOW_HANDLE));
		chart_window_.handle(handle_);
		
		// окно графика лежит внутри другого окна
		select_parent();
	}

	void refresh()
	{
		chart_window_.command(33324);
	}

};

