/*
Copyright 2019 FXcoder

This file is part of VP.

VP is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with VP. If not, see
http://www.gnu.org/licenses/.
*/

// Положение гистограммы. © FXcoder

#property strict

/*
	- направо от левой границы окна
	- налево от правой границы окна
	- налево (наружу) от левой границы
	- направо (наружу) от правой границы
	- направо (внутрь) от левой границы
	- налево (внутрь) от правой границы
*/

enum ENUM_VP_HG_POSITION
{
	VP_HG_POSITION_WINDOW_LEFT = 0,    // Window left
	VP_HG_POSITION_WINDOW_RIGHT = 1,   // Window right
	VP_HG_POSITION_LEFT_OUTSIDE = 2,   // Left outside
	VP_HG_POSITION_RIGHT_OUTSIDE = 3,  // Right outside
	VP_HG_POSITION_LEFT_INSIDE = 4,    // Left inside
	VP_HG_POSITION_RIGHT_INSIDE = 5    // Right inside
};
