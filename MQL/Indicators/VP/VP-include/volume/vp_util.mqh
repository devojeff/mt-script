/*
Copyright 2019 FXcoder

This file is part of VP.

VP is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with VP. If not, see
http://www.gnu.org/licenses/.
*/

// VP utilities. © FXcoder

#property strict

#include "../bsl.mqh"
#include "../class/timer.mqh"
#include "../enum/point_scale.mqh"
#include "enum/vp_bar_style.mqh"
#include "enum/vp_range_mode.mqh"
#include "../s.mqh"

class CVPUtil
{
protected:

	string id_;

	CBGO *line_from_;
	CBGO *line_to_;
	color line_from_color_;            // Left border line color
	color line_to_color_;              // Right border line color
	ENUM_LINE_STYLE line_from_style_;  // Left border line style
	ENUM_LINE_STYLE line_to_style_;    // Right border line style

	color prev_background_color_;

	ENUM_VP_BAR_STYLE hg_bar_style_;
	ENUM_POINT_SCALE hg_point_scale_;
	double hg_point_;
	int hg_point_digits_;
		
	color default_hg_color1_;
	color default_hg_color2_;
	color hg_color1_;
	color hg_color2_;
	int hg_line_width_;

	color mode_color_;
	color max_color_;
	color median_color_;
	color vwap_color_;
	int mode_line_width_;

	ENUM_LINE_STYLE stat_line_style_;

	color mode_level_color_;
	ENUM_LINE_STYLE mode_level_style_;
	int mode_level_width_;

	bool show_hg_;
	bool show_modes_;
	bool show_max_;
	bool show_median_;
	bool show_vwap_;
	bool show_mode_level_;


public:

	_GET(CBGO*, line_from);
	_GET(CBGO*, line_to);
	_GET(double, hg_point);
	_GET(bool, show_modes);
	_GET(bool, show_max);
	_GET(bool, show_median);
	_GET(bool, show_vwap);

	void CVPUtil(
		string id,
		ENUM_POINT_SCALE hg_point_scale,
		ENUM_VP_BAR_STYLE hg_bar_style,
		color hg_color1,
		color hg_color2,
		int   hg_line_width,
		color mode_color,
		color max_color,
		color median_color,
		color vwap_color,
		int   mode_line_width,
		ENUM_LINE_STYLE stat_line_style,
		color mode_level_color,
		int   mode_level_width,
		ENUM_LINE_STYLE mode_level_style
		):
			id_(id),
			hg_bar_style_(hg_bar_style),
			hg_point_scale_(hg_point_scale),
			default_hg_color1_(hg_color1),
			default_hg_color2_(hg_color2),
			hg_line_width_(hg_line_width),
			mode_color_(mode_color),
			max_color_(max_color),
			median_color_(median_color),
			vwap_color_(vwap_color),
			mode_line_width_(mode_line_width),
			stat_line_style_(stat_line_style),
			mode_level_color_(mode_level_color),
			mode_level_style_(mode_level_style),
			mode_level_width_(mode_level_width),
			prev_background_color_(clrNONE),
			hg_color1_(NULL),
			hg_color2_(NULL),
			line_from_color_(clrBlue),
			line_from_style_(STYLE_DASH),
			line_to_color_(clrCrimson),
			line_to_style_(STYLE_DASH)
	{
		hg_point_ = _Point * hg_point_scale_;
		hg_point_digits_ = _conv.point_to_digits(hg_point_, _Digits);
		
		show_hg_         = _color.is_valid(hg_color1_) || _color.is_valid(hg_color2_);
		show_modes_      = _color.is_valid(mode_color_);
		show_max_        = _color.is_valid(max_color_);
		show_median_     = _color.is_valid(median_color_);
		show_vwap_       = _color.is_valid(vwap_color_);
		show_mode_level_ = _color.is_valid(mode_level_color_);
		
		line_from_ = _go[id_ + "-from"]; // range
		line_to_   = _go[id_ + "-to"];   // range
	}

	// нарисовать линию горизонта данных
	void draw_horizon(string line_name, datetime time)
	{
		CBGO hz(line_name, OBJ_VLINE, time, 0);
		hz.colour(clrRed).width(1).style(STYLE_DOT);
		hz.selectable(false).hidden(true).back(false);
		hz.text("VP: no data behind this line");
		hz.tooltip(hz.text());
	}

	// нарисовать бар гистограммы
	void draw_bar(string name, datetime time1, datetime time2, double price,
		color line_color, int width, ENUM_VP_BAR_STYLE bar_style, ENUM_LINE_STYLE line_style, bool back, string tooltip = "\n")
	{
		CBGO bar(name);
		bar.del();
		
		if (bar_style == VP_BAR_STYLE_BAR)
		{
#ifdef __MQL4__
			CBGO bar1(name + "+1");
			CBGO bar2(name + "+2");
			CBGO bar3(name + "+3");

			// имитация прямоугольника из-за невозможности нарисовать в MT4 пустой прямоугольник фоном
			bar.create(OBJ_TREND, 0, time2, price - hg_point_ / 2.0, time2, price + hg_point_ / 2.0);
			bar1.create(OBJ_TREND, 0, time1, price - hg_point_ / 2.0, time2, price - hg_point_ / 2.0).tooltip(tooltip);
			bar2.create(OBJ_TREND, 0, time1, price + hg_point_ / 2.0, time2, price + hg_point_ / 2.0).tooltip(tooltip);
			bar3.create(OBJ_TREND, 0, time1, price - hg_point_ / 2.0, time1, price + hg_point_ / 2.0).tooltip(tooltip);
#else
			bar.create(OBJ_RECTANGLE, 0, time1, price - hg_point_ / 2.0, time2, price + hg_point_ / 2.0);
#endif
		}
		else if ((bar_style == VP_BAR_STYLE_FILLED) || (bar_style == VP_BAR_STYLE_COLOR))
		{
			bar.create(OBJ_RECTANGLE, 0, time1, price - hg_point_ / 2.0, time2, price + hg_point_ / 2.0);
		}
		else if (bar_style == VP_BAR_STYLE_OUTLINE)
		{
			bar.create(OBJ_TREND, 0, time1, price, time2, price + hg_point_);
		}
		else
		{
			bar.create(OBJ_TREND, 0, time1, price, time2, price);
		}
		
		set_bar_style(name, line_color, width, bar_style, line_style, back);
		bar.tooltip(tooltip);

#ifdef __MQL4__
		if (bar_style == VP_BAR_STYLE_BAR)
		{
			set_bar_style(name + "+1", line_color, width, bar_style, line_style, back);
			set_bar_style(name + "+2", line_color, width, bar_style, line_style, back);
			set_bar_style(name + "+3", line_color, width, bar_style, line_style, back);
		}
#endif
	}


	// установить стиль бара
	void set_bar_style(string name, color line_color, int width, ENUM_VP_BAR_STYLE bar_style, ENUM_LINE_STYLE line_style, bool back)
	{
		CBGO bar(name);
		bar.hidden(true).selectable(false);
		bar.line_style(line_style, width, line_color);

#ifdef __MQL4__
		bar.ray(false).ray_right(false);

		/**if (bar_style == VP_BAR_STYLE_BAR)
			back = false;
		else **/if ((bar_style == VP_BAR_STYLE_FILLED) || (bar_style == VP_BAR_STYLE_COLOR))
			back = true;
		
		bar.back(back);
#else
		bar.ray_left(false).ray_right(false);
		bar.back(back).fill((bar_style == VP_BAR_STYLE_FILLED) || (bar_style == VP_BAR_STYLE_COLOR));
#endif
	}


	// нарисовать уровень
	void draw_level(string name, double price, string tooltip = "\n")
	{
		CBGO level(name);
		level.del();
		level.create(OBJ_HLINE, 0, 0, price);
		level.hidden(true).selectable(false);
		level.colour(mode_level_color_).style(mode_level_style_).width(mode_level_style_== STYLE_SOLID ? mode_level_width_ : 1);
		level.tooltip(tooltip);
		
		// не должен быть фоном, иначе не будет отображена цена справа
		level.back(false);
	}

	// нарисовать гистограмму
	void draw_hg(string prefix, double low_price, double &volumes[], int bar_from, int bar_to,
		double zoom, const int &modes_pos[], int max_pos = -1, int median_pos = -1, int vwap_pos = -1)
	{
		if (ArraySize(volumes) == 0)
			return;
		
		if (bar_from > bar_to)
			zoom = -zoom;

//_log.show(VAR(hg_color1_), VAR(hg_color2_));
		color cl = hg_color1_;
		double max_volume = _arr.max_value(volumes);
		
		// Учесть возможность нулевых значений всех баров
		if (max_volume == 0)
			max_volume = 1;
		
		double volume;
		double next_volume = 0;
		bool is_outline = hg_bar_style_ == VP_BAR_STYLE_OUTLINE;
		
		int bar1 = bar_from;
		int bar2 = bar_to;
		int mode_bar2 = bar_to;
		
		for (int i = 0, size = ArraySize(volumes); i < size; i++)
		{
			double price = NormalizeDouble(low_price + i * hg_point_, hg_point_digits_);
			string price_string = DoubleToString(price, hg_point_digits_);
			string name = prefix + price_string;
			
			volume = volumes[i];

			if (is_outline)
			{
				if (i < size - 1)
				{
					next_volume = volumes[i + 1];
					bar1 = (int)(bar_from + volume * zoom);
					bar2 = (int)(bar_from + next_volume * zoom);
					mode_bar2 = bar1;
				}
			}
			else if (hg_bar_style_ != VP_BAR_STYLE_COLOR)
			{
				bar2 = (int)(bar_from + volume * zoom);
				mode_bar2 = bar2;
			}

			datetime time_from  = _series.bar_time(bar_from);
			datetime time_to    = _series.bar_time(bar_to);
			datetime t1         = _series.bar_time(bar1);
			datetime t2         = _series.bar_time(bar2);
			datetime mt2        = _series.bar_time(mode_bar2);

			string tooltip = (string)_math.round_to_long(volume) + " @ " + price_string;
			
		
			// При совпадении нескольких уровней рисовать только один в приоритете: max, median, vwap_pos, mode.
			// Если ничего не совпадает, нарисовать обычный бар.
			
			if (show_mode_level_ && (_arr.contains(modes_pos, i)))
			{
				draw_level(name + " level", price, tooltip);
			}

			// В режиме контура последний бар не рисуется
			if (show_hg_ && !(is_outline && (i == size - 1)))
			{
				if (hg_color1_ != hg_color2_)
					cl = _color.mix(hg_color1_, hg_color2_, (is_outline ? fmax(volume, next_volume) : volume) / max_volume, 8);

//_log.show(VAR(cl), VAR(volume), VAR(next_volume), VAR(max_volume));
				draw_bar(name, t1, t2, price, cl, hg_line_width_, hg_bar_style_, STYLE_SOLID, true, tooltip);
			}
			
			if (show_median_ && (i == median_pos))
			{
				draw_bar(name + " median", time_from, time_to, price, median_color_, mode_line_width_, VP_BAR_STYLE_LINE, stat_line_style_, false, "median " + tooltip);
			}
			else if (show_vwap_ && (i == vwap_pos))
			{
				draw_bar(name + " vwap", time_from, time_to, price, vwap_color_, mode_line_width_, VP_BAR_STYLE_LINE, stat_line_style_, false, "VWAP " + tooltip);
			}
			else if ((show_max_ && (i == max_pos)) || (show_modes_ && (_arr.contains(modes_pos, i))))
			{
				bool is_max = (show_max_ && (i == max_pos));
				color mode_color = is_max ? max_color_ : mode_color_;
				string mode_tooltip = (is_max ? "max " : "mode ") + tooltip;

				if (hg_bar_style_ == VP_BAR_STYLE_LINE)
				{
					draw_bar(name, time_from, mt2, price, mode_color, mode_line_width_, VP_BAR_STYLE_LINE, STYLE_SOLID, false, mode_tooltip);
				}
				else if (hg_bar_style_ == VP_BAR_STYLE_BAR)
				{
					draw_bar(name, time_from, mt2, price, mode_color, mode_line_width_, VP_BAR_STYLE_BAR, STYLE_SOLID, false, mode_tooltip);
				}
				else if (hg_bar_style_ == VP_BAR_STYLE_FILLED)
				{
					draw_bar(name, time_from, mt2, price, mode_color, mode_line_width_, VP_BAR_STYLE_FILLED, STYLE_SOLID, false, mode_tooltip);
				}
				else if (hg_bar_style_ == VP_BAR_STYLE_OUTLINE)
				{
					draw_bar(name + "+", time_from, mt2, price, mode_color, mode_line_width_, VP_BAR_STYLE_LINE, STYLE_SOLID, false, mode_tooltip);
				}
				else if (hg_bar_style_ == VP_BAR_STYLE_COLOR)
				{
					draw_bar(name, time_from, mt2, price, mode_color, mode_line_width_, VP_BAR_STYLE_FILLED, STYLE_SOLID, false, mode_tooltip);
				}
			}
		}
	}

	// получить диапазон баров в текущем ТФ (для рисования)
	bool get_range_bars(datetime time_from, datetime time_to, int &bar_from, int &bar_to)
	{
		bar_from = _series.time_bar_right(time_from);
		bar_to = _series.time_bar_right(time_to);
		return(true);
	}

	int smooth_hg(int depth, double &vh[])
	{
		int v_count = ArraySize(vh);
		if (depth == 0)
			return(v_count);

		// расширить массив (необходимо для корректных расчетов)
		int new_count = v_count + 2 * depth;
		
		// сдвинуть значения и занулить хвосты
		double th[];
		ArrayResize(th, new_count);
		ArrayInitialize(th, 0);

		ArrayCopy(th, vh, depth, 0);

		ArrayResize(vh, new_count);
		ArrayInitialize(vh, 0);

		// последовательное усреднение
		for (int d = 0; d < depth; d++)
		{
			for (int i = -d; i < v_count + d; i++)
				vh[i + depth] = (th[i + depth - 1] + th[i + depth] + th[i + depth + 1]) / 3.0;
			
			ArrayCopy(th, vh);
		}

		ArrayResize(vh, v_count);
		ArrayCopy(vh, th, 0, depth, v_count);

		return(new_count);
	}


	// Обновить цвета, вычисляемые автоматически. Если обновление произошло, вернуть true, иначе false
	bool update_auto_colors()
	{
		if (!show_hg_)
			return(false);

		bool is_none1 = _color.is_none(default_hg_color1_);
		bool is_none2 = _color.is_none(default_hg_color2_);
		if (is_none1 && is_none2)
			return(false);

		color new_bg_color = _chart.color_background();
		if (new_bg_color == prev_background_color_)
			return(false);
			
		hg_color1_ = is_none1 ? new_bg_color : default_hg_color1_;
		hg_color2_ = is_none2 ? new_bg_color : default_hg_color2_;

		prev_background_color_ = new_bg_color;
		return(true);
	}

	void draw_line_from(datetime time_from)
	{
		line_from_.create(OBJ_VLINE, 0, time_from, 0);
		line_from_.colour(line_from_color_).back(false).style(line_from_style_).width(1);
		line_from_.tooltip("VP range line");
	}

	void draw_line_to(datetime time_to)
	{
		line_to_.create(OBJ_VLINE, 0, time_to, 0);
		line_to_.colour(line_to_color_).back(false).style(line_to_style_).width(1);
		line_to_.tooltip("VP range line");
	}

	void draw_range_lines(datetime time_from, datetime time_to)
	{
		draw_line_from(time_from);
		draw_line_to(time_to);
	}

	void enable_line_from()
	{
		line_from_.selectable(true).hidden(false);
	}

	void enable_line_to()
	{
		line_to_.selectable(true).hidden(false);
	}

	void enable_range_lines()
	{
		enable_line_from();
		enable_line_to();
	}

	void disable_line_from()
	{
		line_from_.selectable(false).hidden(true);
	}

	void disable_line_to()
	{
		line_to_.selectable(false).hidden(true);
	}

	void disable_range_lines()
	{
		disable_line_from();
		disable_line_to();
	}

	void delete_range_lines()
	{
		line_from_.del();
		line_to_.del();
	}

};

