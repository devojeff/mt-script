/*
Copyright 2019 FXcoder

This file is part of VP.

VP is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with VP. If not, see
http://www.gnu.org/licenses/.
*/

// Класс выбора объектов в стиле _go[obj_name]. Better Standard Library. © FXcoder

#property strict

#include "type/array/arrayptrt.mqh"
#include "go.mqh"


class CBGOSelect
{
private:

	long chart_id_;
	CBArrayPtrT<CBGO> objects_;


public:

	void CBGOSelect():
		chart_id_(0)
	{
	}

	void CBGOSelect(long chart_id):
		chart_id_(chart_id)
	{
	}

	CBGO *operator[](string name)
	{
		for (int i = objects_.size() - 1; i >= 0; i--)
		{
			if (objects_.data[i].name() == name)
				return(objects_.data[i]);
		}
		
		return(objects_.add_return(new CBGO(chart_id_, name)));
	}

} _go;

