/*
Copyright 2019 FXcoder

This file is part of VP.

VP is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with VP. If not, see
http://www.gnu.org/licenses/.
*/

// Свойства события графика OnChartEvent. Better Standard Library. © FXcoder

#property strict

#include "util/math.mqh"
#include "util/time.mqh"
#include "chart.mqh"


class CBChartEvent
{
private:

	const int    id_;
	const long   lparam_;
	const double dparam_;
	const string sparam_;

	// время предыдущего клика в мс для определения двойного клика
	static long            chart_dbl_click_prev_click_time_msc_;
	static string          chart_dbl_click_prev_symbol_;
	static ENUM_TIMEFRAMES chart_dbl_click_prev_period_;

public:

	void CBChartEvent(int id, long lparam, double dparam, string sparam):
		id_(id), lparam_(lparam), dparam_(dparam), sparam_(sparam)
	{
	}

	long   long_param   () const { return(lparam_); }
	double double_param () const { return(dparam_); }
	string string_param () const { return(sparam_); }


	bool is_click_event()        const { return(id_ == CHARTEVENT_CLICK); }
	bool is_chart_change_event() const { return(id_ == CHARTEVENT_CHART_CHANGE); }

	// вызов должен производиться только из одного места
	bool is_double_click_event(int gap_msc = 333) const
	{
		if (!is_click_event())
			return(false);

		long time = _time.tick_count_long();

		//if (time == chart_dbl_click_prev_click_time_msc_)
		//	return(false);

		bool res = time - chart_dbl_click_prev_click_time_msc_ <= gap_msc;
		// без этого нельзя располагать переключатели символов или таймфреймов в зонах двойного клика
		res = res && (chart_dbl_click_prev_symbol_ == _Symbol);
		res = res && (chart_dbl_click_prev_period_ == _Period);
		
		chart_dbl_click_prev_click_time_msc_ = time;
		chart_dbl_click_prev_symbol_ = _Symbol;
		chart_dbl_click_prev_period_ = (ENUM_TIMEFRAMES)_Period;
		return(res);
	}


	bool is_mouse_move_event() const
	{
		return(id_ == CHARTEVENT_MOUSE_MOVE);
	}

	bool is_mouse_move_event(int &mouse_x, int &mouse_y) const
	{
		if (id_ != CHARTEVENT_MOUSE_MOVE)
			return(false);
		
		mouse_x = mouse_x();
		mouse_y = mouse_y();
		return(true);
	}

	// mouse_y will be relative to window
	bool is_mouse_move_event(int window, int &mouse_x, int &mouse_y) const
	{
		if (id_ != CHARTEVENT_MOUSE_MOVE)
			return(false);
		
		mouse_x = mouse_x();
		if (!_math.is_in_range(mouse_x, 0, _chart.width_in_pixels() - 1))
			return(false);

		mouse_y = mouse_y(window);
		if (!_math.is_in_range(mouse_y, 0, _chart.height_in_pixels(window) - 1))
			return(false);
			
		return(true);
	}

	bool is_custom_event() const
	{
		return((id_ >= CHARTEVENT_CUSTOM) && (id_ <= CHARTEVENT_CUSTOM_LAST));
	}

	bool is_custom_event(int event_n) const
	{
		return((event_n >= 0) && (event_n <= USHORT_MAX) && (id_ == CHARTEVENT_CUSTOM + event_n));
	}

	// подразумеваются все действия, изменяющие координаты, включая создание и удаление объекта
	bool is_object_move_event() const
	{
		return
		(
			(id_ == CHARTEVENT_OBJECT_CREATE) ||
			(id_ == CHARTEVENT_OBJECT_CHANGE) ||
			(id_ == CHARTEVENT_OBJECT_DRAG)   ||
			(id_ == CHARTEVENT_OBJECT_DELETE)
		);
	}

	// подразумеваются все действия, изменяющие координаты, включая создание и удаление объекта
	bool is_object_move_event(string name) const
	{
		return(is_object_move_event() && (sparam_ == name));
	}

	bool is_key_down_event() const
	{
		return(id_ == CHARTEVENT_KEYDOWN);
	}

	bool is_key_down_event(ushort key) const
	{
		return(is_key_down_event() && (key == lparam_));
	}

	bool is_object_click_event() const
	{
		return(id_ == CHARTEVENT_OBJECT_CLICK);
	}

	bool is_object_click_event(string name) const
	{
		return(is_object_click_event() && (sparam_ == name));
	}

	bool is_object_click_event_prefix(string prefix) const
	{
		bool res = (id_ == CHARTEVENT_OBJECT_CLICK);
		
		if (prefix == "")
			return(res);
		
		return(res && (::StringFind(sparam_, prefix) == 0));
	}


	int mouse_x() const
	{
		return(int(lparam_));
	}

	int mouse_y() const
	{
		return(int(dparam_));
	}

	int mouse_y(int window) const
	{
		return(int(dparam_) - _chart.window_y_distance(window));
	}

	ushort key() const
	{
		return(ushort(lparam_));
	}

	string key_string() const
	{
		return(::ShortToString(this.key()));
	}

};

long               CBChartEvent::chart_dbl_click_prev_click_time_msc_     = 0;
string             CBChartEvent::chart_dbl_click_prev_symbol_             = "";
ENUM_TIMEFRAMES    CBChartEvent::chart_dbl_click_prev_period_             = PERIOD_CURRENT;

