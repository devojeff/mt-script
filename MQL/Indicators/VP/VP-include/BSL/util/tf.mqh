/*
Copyright 2019 FXcoder

This file is part of VP.

VP is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with VP. If not, see
http://www.gnu.org/licenses/.
*/

// Битовые функции. Better Standard Library. © FXcoder

#property strict


class CBTimeframeUtil
{
public:

	static const int minutes; // число минут в баре текущего таймфрейма
	static const int seconds; // число секунд в баре текущего таймфрейма

	static const ENUM_TIMEFRAMES list[]; // список всех доступных таймфреймов
	static const int             count;  // число всех доступных таймфреймов


public:

	/**
	Преобразовать период (таймфрейм) в строковое представление.
	@param tf  Таймфрейм. Если не указан, используется текущий.
	@return    Строковое представление таймфрейма. Могут быть использованы таймфреймы, отсутствующие в терминале.
	*/
	static string to_string(ENUM_TIMEFRAMES tf = PERIOD_CURRENT)
	{
		int period = ::PeriodSeconds(tf) / 60;
		
		if (period % 43200 == 0)
			return("MN" + ::IntegerToString(period / 43200));
		else if (period % 10080 == 0)
			return("W" + ::IntegerToString(period / 10080));
		else if (period % 1440 == 0)
			return("D" + ::IntegerToString(period / 1440));
		else if (period % 60 == 0)
			return("H" + ::IntegerToString(period / 60));
		else
			return("M" + ::IntegerToString(period));
	}

#ifdef __MQL4__
	// Для использования с 0, NULL, _Period (int в 4)
	static string to_string(int tf) { return(to_string((ENUM_TIMEFRAMES)tf)); }
#endif

	static int to_minutes(ENUM_TIMEFRAMES tf)
	{
		return(::PeriodSeconds(tf) / 60);
	}

	static int to_seconds(ENUM_TIMEFRAMES tf)
	{
		return(::PeriodSeconds(tf));
	}

#ifdef __MQL4__
	// Для использования с 0, NULL, _Period (int в 4)
	static int to_minutes(int tf) { return(to_minutes((ENUM_TIMEFRAMES)tf)); }
	static int to_seconds(int tf) { return(to_seconds((ENUM_TIMEFRAMES)tf)); }
#endif

	// Получить ближайший таймфрейм к заданному в минутах.
	// Находится всегда.
	static ENUM_TIMEFRAMES find_closest(int min)
	{
		long sec = min * 60;
		long min_diff = INT_MAX;
		ENUM_TIMEFRAMES tf = PERIOD_CURRENT; // remove `possible use of uninitialized variable`
		
		for (int i = 0; i < _tf.count; i++)
		{
			long tf_sec = ::PeriodSeconds(list[i]);
			long diff = ::fabs(tf_sec - sec);
			
			if (diff <= min_diff) // отдавать предпочтение большему ТФ (3 кажется ближе к 5 чем к 1)
			{
				min_diff = diff;
				tf = list[i];
			}
		}
		
		return(tf);
	}

};

const int CBTimeframeUtil::minutes = ::PeriodSeconds() / 60;
const int CBTimeframeUtil::seconds = ::PeriodSeconds();


#ifdef __MQL4__

	const ENUM_TIMEFRAMES CBTimeframeUtil::list[] =
	{
		PERIOD_M1, PERIOD_M5, PERIOD_M15, PERIOD_M30,
		PERIOD_H1, PERIOD_H4,
		PERIOD_D1, PERIOD_W1, PERIOD_MN1
	};

	const int CBTimeframeUtil::count = 9;

#else

	const ENUM_TIMEFRAMES CBTimeframeUtil::list[] =
	{
		PERIOD_M1, PERIOD_M2, PERIOD_M3, PERIOD_M4, PERIOD_M5, PERIOD_M6, PERIOD_M10, PERIOD_M12, PERIOD_M15, PERIOD_M20, PERIOD_M30,
		PERIOD_H1, PERIOD_H2, PERIOD_H3, PERIOD_H4, PERIOD_H6, PERIOD_H8, PERIOD_H12,
		PERIOD_D1, PERIOD_W1, PERIOD_MN1
	};

	const int CBTimeframeUtil::count = 21;

#endif


CBTimeframeUtil _tf;

