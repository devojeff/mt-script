/*
Copyright 2019 FXcoder

This file is part of VP.

VP is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with VP. If not, see
http://www.gnu.org/licenses/.
*/

/*
Отладка. Better Standard Library. © FXcoder
Работа зависит от определений DEBUG, DEBUG_WARNING_PRINT, DEBUG_WARNING_ALERT.
*/

#property strict

// В режиме отладки терминала включать макрос отладки автоматически
#ifdef _DEBUG
	#ifndef DEBUG
		#define DEBUG
	#endif
#endif


#define FUNC_LINE_PREFIX      (string(__FUNCTION__) + "," + string(__LINE__) + ": ")
#define ERROR_SUFFIX          (_LastError > 0 ? (" [" + _err.last_message(false) + "]") : "")
#define DEBUG_MESSAGE(M)      (string(FUNC_LINE_PREFIX) + string(M) + (ERROR_SUFFIX))

#define VV(V)                 (#V + "=" + string(V))
#define VVD(V, D)             (#V + "=" + ::DoubleToString((V), (D)))
#define VVDC(V)               (#V + "=" + _double.to_string_compact(V))
#define VVE(V)                (#V + "=" + ::EnumToString(V))
#define VVPAD(V, P)           (#V + "=" + _str.pad(string(V), P))

#define VAR(V)                ("  " + VV(V))
#define VARD(V, D)            ("  " + VVD(V, D))
#define VARDC(V)              ("  " + VVDC(V))
#define VARE(V)               ("  " + VVE(V))
#define VARPAD(V, P)          ("  " + VVPAD(V, P))


#ifdef DEBUG

#define PRINT(M)              { ::Print(DEBUG_MESSAGE(M)); }
#define PRINT_RET(M)          { ::Print(DEBUG_MESSAGE(M)); return; }
#define PRINT_RETURN(M, R)    { ::Print(DEBUG_MESSAGE(M)); return(R); }
#define PRINT_BREAK(M)        { ::Print(DEBUG_MESSAGE(M)); break; }

#define LOG(M)                { _log.show(M); }

#else

#define PRINT(M)              { }
#define PRINT_RET(M)          { return; }
#define PRINT_RETURN(M, R)    { return(R); }
#define PRINT_BREAK(M)        { break; }

#define LOG(M)                { }

#endif

#define ASSERT(C)             { if (!(C)) PRINT        ("Assertion failed: " + #C); }
#define ASSERT_RET(C)         { if (!(C)) PRINT_RET    ("Assertion failed: " + #C); }
#define ASSERT_RETURN(C, R)   { if (!(C)) PRINT_RETURN ("Assertion failed: " + #C, (R)); }


/* Предупреждения */

//#define DEBUG_ALERT

// Способ предупреждений по умолчанию в режиме отладки
#ifdef DEBUG
	#ifndef DEBUG_ALERT
		#ifndef DEBUG_PRINT
			#define DEBUG_PRINT
		#endif
	#endif
#endif


#include "util/double.mqh"


class CBDebug
{
public:

	void warning(string message)
	{
#ifdef DEBUG_PRINT
		::Print(message);
		return;
#endif

#ifdef DEBUG_ALERT
		::Alert(message);
		return;
#endif

	}

} _debug;
