/*
Copyright 2019 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

#property copyright "MultiStochastic v8.0.1. © FXcoder"
#property link      "https://fxcoder.blogspot.com"
#property strict

// todo: собственные расчёты

#define MAX_PERIOD 10000
#define MAX_PLOTS 11

#property indicator_separate_window
#property indicator_buffers MAX_PLOTS
#property indicator_plots MAX_PLOTS

#property indicator_color1 clrDodgerBlue
#property indicator_color2 C'62,123,218'
#property indicator_color3 C'94,102,182'
#property indicator_color4 C'126,82,145'
#property indicator_color5 C'158,61,109'
#property indicator_color6 C'190,41,72'
#property indicator_color7 C'222,20,36'
#property indicator_color8 clrRed

#property indicator_minimum 0
#property indicator_maximum 100


#include "MultiStoch-include/bsl.mqh"
#include "MultiStoch-include/enum/gradient.mqh"
#include "MultiStoch-include/enum/step.mqh"
#include "MultiStoch-include/enum/multi_theme.mqh"
#include "MultiStoch-include/plot/gradientlineplotset.mqh"
#include "MultiStoch-include/util/indicator.mqh"
#include "MultiStoch-include/util/multi.mqh"


input int               LineCount = 8;                // Line Count

input string            gr_period_ = "";              // •••••••••• PERIODS ••••••••••
input int               PeriodStart = 3;              // Start
input int               PeriodEnd = 89;               // End
input ENUM_STEP         PeriodStepType = STEP_EXP;    // Step
input string            Periods = "";                 // Custom (overrides above params)

input string            gr_slowing_ = "";             // •••••••••• SLOWING PERIODS ••••••••••
input int               SlowingStart = 1;             // Start
input int               SlowingEnd = 5;               // End
input ENUM_STEP         SlowingStepType = STEP_EXP;   // Step
input string            Slowings = "";                // Custom (overrides above params)

input string            gr_source_ = "";              // •••••••••• SOURCE ••••••••••
input string            Sym = "";                     // Symbol
input bool              Reverse = false;              // Reverse
input ENUM_STO_PRICE    PriceType = STO_LOWHIGH;      // Price Type

input string            gr_visual_ = "";                   // •••••••••• VISUAL ••••••••••
input ENUM_MULTI_THEME  Theme = MULTI_THEME_RED_DDGBLUE;   // Theme
input ENUM_GRADIENT     GradientType = GRADIENT_RGB;       // Gradient Type
input color             StartColor = Red;                  // Start Color
input color             MidColor = clrNONE;                // Middle Color (None = do not use)
input color             EndColor = clrDodgerBlue;          // End Color
input bool              StartInFront = true;               // Start in Front
input int               LineWidth = 1;                     // Line Width (0 = do not set)

input string            gr_etc_ = "";       // •••••••••• ETC ••••••••••
input int               Shift = 0;          // Shift
input int               MaxBars = 5000;     // Maximum Number of Bars


int periods_[], slowings_[]; // наборы периодов
bool is_chart_symbol_; // является ли расчетный символ символом чарта

int nperiods_, period_start_, period_end_, slowing_start_, slowing_end_;
string symbol_;
CBSymbol sym_(Sym);
CBSeries ser_(Sym, PERIOD_CURRENT);
int drawing_begin_;

CGradientLinePlotSet plots_;
int handles_[];
bool is_initialized_ = false;


void OnInit()
{
	is_initialized_ = init_source() && init_periods() && init_plots() && init_indicators(); // keep order

	if (!is_initialized_)
	{
		_indicator.short_name(_mql.program_name() + ": Wrong parameters.");
		return;
	}

	_indicator.short_name(_mql.program_name() + ": " +
		(Reverse ? "-" : "") + symbol_ +
		"(" +
			_conv.range_to_string(periods_, enum_step_to_string(PeriodStepType, true)) +
			"," +
			_conv.range_to_string(slowings_, enum_step_to_string(SlowingStepType, true)) +
		")");
}

int OnCalculate(const int rates_total, const int prev_calculated, const datetime &time[], const double &open[], const double &high[], const double &low[], const double &close[], const long &tick_volume[], const long &volume[], const int &spread[])
{
	if (!is_initialized_)
		return(0);

#ifndef __MQL4__
	ArraySetAsSeries(time, true);
#endif

	int bars_to_calc = get_bars_to_calculate(rates_total, symbol_, prev_calculated, MaxBars, is_chart_symbol_ ? 1 : 5, drawing_begin_);
	int calculated_bars = rates_total - bars_to_calc;

	plots_.empty(prev_calculated == 0 ? -1 : bars_to_calc);
	int nbars = _series.bars();
	
	// rates are not loaded
	if (nbars != plots_.size())
		return(0);

	for (int i = bars_to_calc - 1; i >= 0; i--)
	{
		int pbar = nbars - 1 - i;
		double stoch = EMPTY_VALUE;

#ifdef __MQL4__

		// Если исходные данные с другого инструмента, необходимо определить бар отсчета по
		// времени текущего бара текущего инструмента со сдвигом влево.
		int bar = is_chart_symbol_ ? i : ser_.bar_shift(time[i]);
		ResetLastError();

		for (int p = 0; p < nperiods_; p++)
		{
			stoch = iStochastic(symbol_, 0, periods_[p], 1, slowings_[p], MODE_SMA, PriceType, 0, bar);

			if (GetLastError() != ERR_NO_ERROR)
			{
				stoch = EMPTY_VALUE;
				break;
			}

			plots_[p].buffer[pbar] = Reverse ? (100.0 - stoch) : stoch;
		}

#else

		for (int p = 0; p < nperiods_; p++)
		{
			stoch = indicator_value_by_time(handles_[p], time[i]);
			if (stoch == EMPTY_VALUE)
				break;

			plots_[p].buffer[pbar] = Reverse ? (100.0 - stoch) : stoch;
		}

#endif
		
		if (stoch == EMPTY_VALUE)
		{
			plots_.empty_bar(pbar);
			break;
		}
			
		calculated_bars++;
	}

	return(calculated_bars);
}


void OnDeinit(const int reason)
{
#ifndef __MQL4__
	indicators_release(handles_);
#endif
}

bool init_source()
{
	symbol_ = sym_.name();
	is_chart_symbol_ = symbol_ == _Symbol;
	return(sym_.exists());
}

bool init_periods()
{
	nperiods_ = _math.put_in_range(LineCount, 1, MAX_PLOTS);
	
	period_start_ = _math.put_in_range(PeriodStart, 1, MAX_PERIOD);
	period_end_ = _math.put_in_range(PeriodEnd, 1, MAX_PERIOD);
	
	slowing_start_ = _math.put_in_range(SlowingStart, 1, MAX_PERIOD);
	slowing_end_ = _math.put_in_range(SlowingEnd, 1, MAX_PERIOD);

	// Расчет периодов
	nperiods_ = multi_get_periods(period_start_, period_end_, nperiods_, PeriodStepType, Periods, periods_);
	if (nperiods_ <= 0)
		return(false);
	
	int nslowings = multi_get_periods(slowing_start_, slowing_end_, nperiods_, SlowingStepType, Slowings, slowings_);
	if (nperiods_ != nslowings)
	{
		if (nslowings != 1)
			return(false);
		
		// Если указан только один период, считать его одинаковым для всех
		int slowing = slowings_[0];
		ArrayResize(slowings_, nperiods_);
		ArrayInitialize(slowings_, slowing);
	}

	if (StartInFront)
	{
		_arr.reverse(periods_);
		_arr.reverse(slowings_);
	}

	return(true);
}

bool init_plots()
{
	if (!plots_.init(0, 0, nperiods_, Theme, 0, StartColor, MidColor, EndColor, GradientType, StartInFront).is_initialized())
		return(false);
		
	plots_.shift(Shift).line_width(LineWidth > 0 ? LineWidth : -1);//.show_data(false);

	drawing_begin_ = _arr.max_value(periods_) + _arr.max_value(slowings_);

#ifdef __MQL4__
	plots_.draw_begin(drawing_begin_ + Shift);
#else
	plots_.draw_begin(drawing_begin_).show_data(false);
#endif

	plots_.disable_unused(MAX_PLOTS);

	return(true);
}

bool init_indicators()
{
#ifndef __MQL4__

	ArrayResize(handles_, nperiods_);
	
	// в 5 без выбора символа в обзоре не рабоатет, выбрать
	sym_.select(true);

	for (int p = 0; p < nperiods_; p++)
		handles_[p] = iStochastic(symbol_, _Period, periods_[p], 1, slowings_[p], MODE_SMA, PriceType);

#endif

	return(true);
}

/*
Список изменений:

8.0:
	* ExpPeriodStep -> PeriodStepType, ExpSlowingStep -> SlowingStepType: выбор из нескольких типов шагов периода
	* новые параметры отображения: Theme (цветовая тема), GradientType (тип градиента), MidColor (средний цвет)
	* новый параметр Slowings для указания периодов Slowing вручную
	* при неверных входных параметрах индикатор теперь не отваливается, а лишь сообщает об ошибке (в заголовке)
	* удалён параметр SlowingMethod, т.к. он не влияет на расчёты

7.1:
	* изменены ссылки на сайт
	* список изменений в коде

7.0:
    * вариант для MT5
    * тип параметра SlowingMethod изменён на ENUM_MA_METHOD

6.0:
	* из имени файла убран префикс "+"
	* изменены ссылки на сайт
	* оптимизация
	* изменены отображаемые названия параметров
	* добавлен параметр Price type (тип цены для расчёта: Low/High или Close/Close)

5.2:
	* совместимость с MetaTrader версии 4.00 Build 600 и новее

5.1:
	* минимальная совместимость с новым MQL4
*/
