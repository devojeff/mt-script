/*
Copyright 2019 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Класс и его глобальный экземпляр для доступа к свойствам индикатора, 5.1880/4.1601. Better Standard Library. © FXcoder

#property strict


#include "mql.mqh"


class CBIndicator
{
protected:

	static const int max_levels_;

	string short_name_;
	int    digits_;
	double maximum_;
	double minimum_;


public:

	// конструктор по умолчанию
	void CBIndicator() { init_properties(); }

	string short_name() const { return(short_name_); }

	CBIndicator *digits      (            int             value ) { digits_ = value;     return(set( INDICATOR_DIGITS,            value )); }  // Точность отображения значений индикатора
	CBIndicator *height      (            int             value ) {                      return(set( INDICATOR_HEIGHT,            value )); }  // Фиксированная высота собственного окна индикатора
	CBIndicator *levels      (            int             value ) {                      return(set( INDICATOR_LEVELS,            value )); }  // Количество уровней на окне индикатора
	CBIndicator *level_text  ( int index, string          value ) {                      return(set( INDICATOR_LEVELTEXT,  index, value )); }  // Описание уровня
	CBIndicator *level_value ( int index, double          value ) {                      return(set( INDICATOR_LEVELVALUE, index, value )); }  // Значение уровня
	CBIndicator *minimum     (            double          value ) { minimum_ = value;    return(set( INDICATOR_MINIMUM,           value )); }  // Минимум окна индикатора
	CBIndicator *maximum     (            double          value ) { maximum_ = value;    return(set( INDICATOR_MAXIMUM,           value )); }  // Максимум окна индикатора
	CBIndicator *short_name  (            string          value ) { short_name_ = value; return(set( INDICATOR_SHORTNAME,         value )); }  // Короткое наименование индикатора

#ifdef __MQL4__
	CBIndicator *level_color  ( color           value ) { return(set( INDICATOR_LEVELCOLOR,        value )); }  // Цвет линий уровней
	CBIndicator *level_style  ( ENUM_LINE_STYLE value ) { return(set( INDICATOR_LEVELSTYLE,        value )); }  // Стиль линий уровней
	CBIndicator *level_width  ( int             value ) { return(set( INDICATOR_LEVELWIDTH,        value )); }  // Толщина линий уровней
#else
	// в 5 цвет, толщина и стиль устанавливаются для каждого уровня отдельно
	// для сомвестимости и удобства пусть будут функции, которые будут устанавливать параметры сразу всем уровням
	CBIndicator *level_color  ( color           value ) { for (int i = 0; i < max_levels_; i++) set( INDICATOR_LEVELCOLOR, i, value ); return(&this); }  // Цвет линий уровней
	CBIndicator *level_style  ( ENUM_LINE_STYLE value ) { for (int i = 0; i < max_levels_; i++) set( INDICATOR_LEVELSTYLE, i, value ); return(&this); }  // Стиль линий уровней
	CBIndicator *level_width  ( int             value ) { for (int i = 0; i < max_levels_; i++) set( INDICATOR_LEVELWIDTH, i, value ); return(&this); }  // Толщина линий уровней
#endif


	int    digits () const { return(digits_);  }  // Точность отображения значений индикатора
	double minimum() const { return(minimum_); }  // Минимум окна индикатора
	double maximum() const { return(maximum_); }  // Максимум окна индикатора


	// Универсальные функции доступа к свойствам

	CBIndicator *set(ENUM_CUSTOMIND_PROPERTY_INTEGER property_id, int    value) { ::IndicatorSetInteger(property_id, value); return(&this); }
	CBIndicator *set(ENUM_CUSTOMIND_PROPERTY_DOUBLE  property_id, double value) { ::IndicatorSetDouble (property_id, value); return(&this); }
	CBIndicator *set(ENUM_CUSTOMIND_PROPERTY_STRING  property_id, string value) { ::IndicatorSetString (property_id, value); return(&this); }

	CBIndicator *set(ENUM_CUSTOMIND_PROPERTY_INTEGER property_id, int modifier, int    value) { ::IndicatorSetInteger(property_id, modifier, value); return(&this); }
	CBIndicator *set(ENUM_CUSTOMIND_PROPERTY_DOUBLE  property_id, int modifier, double value) { ::IndicatorSetDouble (property_id, modifier, value); return(&this); }
	CBIndicator *set(ENUM_CUSTOMIND_PROPERTY_STRING  property_id, int modifier, string value) { ::IndicatorSetString (property_id, modifier, value); return(&this); }


protected:

	void init_properties()
	{
		short_name_ = _mql.program_name();
		digits_ = _Digits;
		maximum_ = 0;
		minimum_ = 0;
	}
};

// всего может быть 32 уровня (не документировано, но больше не принимается через #property indicator_level
const int CBIndicator::max_levels_ = 32;

CBIndicator _indicator;


#ifndef __MQL4__

class CBIndicator5: public CBIndicator
{
public:

	void CBIndicator5(): CBIndicator() { }

	// Свойства
	CBIndicator *level_color  ( int index, color           value ) { return(set( INDICATOR_LEVELCOLOR, index, value )); }  // Цвет линии уровня
	CBIndicator *level_style  ( int index, ENUM_LINE_STYLE value ) { return(set( INDICATOR_LEVELSTYLE, index, value )); }  // Стиль линии уровня
	CBIndicator *level_width  ( int index, int             value ) { return(set( INDICATOR_LEVELWIDTH, index, value )); }  // Толщина линии уровня

	// Возвращает хэндл индикатора. INVALID_HANDLE в случае неудачи
	// Не работает в OnInit()
	int handle()
	{
		if (short_name_ == "")
			return(INVALID_HANDLE);
		
		int window = ::ChartWindowFind();
		int handle = ::ChartIndicatorGet(::ChartID(), window, short_name_);
		return(handle);
	}

} _indicator5;

#endif

