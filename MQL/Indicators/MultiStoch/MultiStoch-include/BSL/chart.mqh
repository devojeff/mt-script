/*
Copyright 2019 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Класс доступа к свойствам графика, 5.1910/4.1601. Better Standard Library. © FXcoder

#property strict

#include "type/chartcolors.mqh"
#include "util/arr.mqh"
#include "util/math.mqh"

#include "chartwindow.mqh"
#include "mql.mqh"


class CBChart
{
protected:

	// может быть отрицательным в целях проверки в цикле, нулём (текущий график) или реальным ID
	long      id_;     // 0 - текущий график (от этого зависит другой код, не менять поведение)


public:

	// Конструктор по умолчанию
	void CBChart():
		id_(0)
	{
	}

	// Конструктор для конкретного графика с указанием Id.
	void CBChart(long chart_id):
		id_(chart_id)
	{
	}

	// Конструктор с открытием графика.
	void CBChart(string symbol, ENUM_TIMEFRAMES timeframe)
	{
		id_ = ::ChartOpen(symbol, timeframe);
	}

	CBChart *close()
	{
		::ChartClose(id_);
		return(&this);
	}

	// функции графика
	CBChart *redraw()
	{
		::ChartRedraw(id_);
		return(&this);
	}

	// Последние 6 десятичных знаков ID, достаточная уникальность при экономии места (например, в именах файлов и ГП)
	int id_short() const
	{
		return(int(id() % 1000000));
	}

	bool xy_to_time_price(int x, int y, int &window, datetime &time, double &price) const
	{
		return(::ChartXYToTimePrice(id_, x, y, window, time, price));
	}

	bool save_template(string name) const
	{
		return(::ChartSaveTemplate(id_, name));
	}

	bool apply_template(string name) const
	{
		return(::ChartApplyTemplate(id_, name));
	}

	bool navigate(ENUM_CHART_POSITION location, int bars_to_navigate = 0) const
	{
		return(::ChartNavigate(id_, location, bars_to_navigate));
	}

	bool navigate_begin(int bars_to_navigate = 0) const
	{
		return(::ChartNavigate(id_, CHART_BEGIN, bars_to_navigate));
	}

	bool navigate_current_pos(int bars_to_navigate = 0) const
	{
		return(::ChartNavigate(id_, CHART_CURRENT_POS, bars_to_navigate));
	}

	bool navigate_end(int bars_to_navigate = 0) const
	{
		return(::ChartNavigate(id_, CHART_END, bars_to_navigate));
	}

	int window_find()
	{
		return(::ChartWindowFind());
	}

	int window_find(string indicator_shortname)
	{
		return(::ChartWindowFind(id_, indicator_shortname));
	}

	bool exists()
	{
		if (id_ < 0)
			return(false);

		if (id_ == 0)
			return(true);
			
		long tmp;
		return(::ChartGetInteger(id_, CHART_WINDOW_HANDLE, 0, tmp));
	}

	void event_custom(ushort event_n, long lparam, double dparam, string sparam)
	{
		::EventChartCustom(id_, event_n, lparam, dparam, sparam);
	}

	void screenshot(string path, int width, int height, ENUM_ALIGN_MODE align = ALIGN_RIGHT)
	{
		::ChartScreenShot(id_, path, width, height, align);
	}

	// при <0 вернуть id_
	long id() const
	{
		return(id_ == 0 ? ::ChartID() : id_);
	}

	void id(long chart_id)
	{
		id_ = chart_id;
	}

	void first()
	{
		id_ = ::ChartFirst();
	}

	long next()
	{
		return(::ChartNext(id_));
	}

	// Для цикла типа for(CBChart chart; chart.loop();)
	bool loop()
	{
		// поведение ChartNext при отрицательном id не документировано, поэтому проверить явно
		if (id_ < 0)
			return(false);
		
		id_ = id_ == 0 ? ::ChartFirst() : ::ChartNext(id_);
		return(id_ > 0);
	}

	// Для цикла типа for(CBChart chart; chart.loop_real();)
	// Только реальные графики (не объекты).
	// В 4 равнозначно loop().
	bool loop_real()
	{
#ifdef __MQL4__
		return(loop());
#else
		while(loop())
		{
			if (!is_object())
				return(true);
		}
		
		return(false);
#endif
	}

	bool is_this_chart() const
	{
		// для скорости в циклах перебора с вызовом этой функции
		if (id_ < 0)
			return(false);
		
		return((id_ == 0) || (id_ == ::ChartID()));
	}


	int objects_total()                                    const { return(::ObjectsTotal(id_)); }
	int objects_total(int subwindow)                       const { return(::ObjectsTotal(id_, subwindow)); }
	int objects_total(int subwindow, ENUM_OBJECT obj_type) const { return(::ObjectsTotal(id_, subwindow, obj_type)); }

	string object_name(int index)                                      const { return(::ObjectName(id_, index)); }
	string object_name(int index, int subwindow)                       const { return(::ObjectName(id_, index, subwindow)); }
	string object_name(int index, int subwindow, ENUM_OBJECT obj_type) const { return(::ObjectName(id_, index, subwindow, obj_type)); }


	int objects_delete_all(int subwindow = -1, int object_type = -1) const
	{
		return(::ObjectsDeleteAll(id_, subwindow, object_type));
	}

	int objects_delete_all(string prefix, int subwindow = -1, int object_type = -1) const
	{
		return(::ObjectsDeleteAll(id_, prefix, subwindow, object_type));
	}

	// symbol & period
	CBChart *symbol(string symbol)
	{
		::ChartSetSymbolPeriod(id_, symbol, ::ChartPeriod(id_));
		return(&this);
	}

	CBChart *period(ENUM_TIMEFRAMES period)
	{
		::ChartSetSymbolPeriod(id_, ::ChartSymbol(id_), period);
		return(&this);
	}

	CBChart *symbol_period(string symbol, ENUM_TIMEFRAMES period)
	{
		::ChartSetSymbolPeriod(id_, symbol, period);
		return(&this);
	}

	string symbol() const
	{
		return(::ChartSymbol(id_));
	}

	ENUM_TIMEFRAMES period() const
	{
		return(::ChartPeriod(id_));
	}

	// установка тех же символа и тф приводит к обновлению графика (но не в точности как по меню Обновить)
	CBChart *refresh()
	{
		return(symbol_period(symbol(), period()));
	}


	// Функции доступа к конкретным свойствам и их комбинациям

	// properties
	CBChart *mode                (ENUM_CHART_MODE        value) { return(set(CHART_MODE,         value)); }
	CBChart *show_volumes        (ENUM_CHART_VOLUME_MODE value) { return(set(CHART_SHOW_VOLUMES, value)); }
	CBChart *height_in_pixels    (int window, int value) { return(set(CHART_HEIGHT_IN_PIXELS, window, value)); }
	CBChart *autoscroll          (bool   value) { return(set( CHART_AUTOSCROLL,          value )); }
	CBChart *bring_to_top        (bool   value) { return(set( CHART_BRING_TO_TOP,        value )); }
	CBChart *color_ask           (color  value) { return(set( CHART_COLOR_ASK,           value )); }
	CBChart *color_background    (color  value) { return(set( CHART_COLOR_BACKGROUND,    value )); }
	CBChart *color_bid           (color  value) { return(set( CHART_COLOR_BID,           value )); }
	CBChart *color_candle_bear   (color  value) { return(set( CHART_COLOR_CANDLE_BEAR,   value )); }
	CBChart *color_candle_bull   (color  value) { return(set( CHART_COLOR_CANDLE_BULL,   value )); }
	CBChart *color_chart_down    (color  value) { return(set( CHART_COLOR_CHART_DOWN,    value )); }
	CBChart *color_chart_line    (color  value) { return(set( CHART_COLOR_CHART_LINE,    value )); }
	CBChart *color_chart_up      (color  value) { return(set( CHART_COLOR_CHART_UP,      value )); }
	CBChart *color_foreground    (color  value) { return(set( CHART_COLOR_FOREGROUND,    value )); }
	CBChart *color_grid          (color  value) { return(set( CHART_COLOR_GRID,          value )); }
	CBChart *color_last          (color  value) { return(set( CHART_COLOR_LAST,          value )); }
	CBChart *color_stop_level    (color  value) { return(set( CHART_COLOR_STOP_LEVEL,    value )); }
	CBChart *color_volume        (color  value) { return(set( CHART_COLOR_VOLUME,        value )); }
	CBChart *comment             (string value) { return(set( CHART_COMMENT,             value )); }
	CBChart *drag_trade_levels   (bool   value) { return(set( CHART_DRAG_TRADE_LEVELS,   value )); }
	CBChart *foreground          (bool   value) { return(set( CHART_FOREGROUND,          value )); }
	CBChart *event_mouse_move    (bool   value) { return(set( CHART_EVENT_MOUSE_MOVE,    value )); }
	CBChart *event_object_create (bool   value) { return(set( CHART_EVENT_OBJECT_CREATE, value )); }
	CBChart *event_object_delete (bool   value) { return(set( CHART_EVENT_OBJECT_DELETE, value )); }
	CBChart *fixed_max           (double value) { return(set( CHART_FIXED_MAX,           value )); }
	CBChart *fixed_min           (double value) { return(set( CHART_FIXED_MIN,           value )); }
	CBChart *fixed_position      (double value) { return(set( CHART_FIXED_POSITION,      value )); }
	CBChart *keyboard_control    (bool   value) { return(set( CHART_KEYBOARD_CONTROL,    value )); }
	CBChart *mouse_scroll        (bool   value) { return(set( CHART_MOUSE_SCROLL,        value )); }
	CBChart *points_per_bar      (double value) { return(set( CHART_POINTS_PER_BAR,      value )); }
	CBChart *quick_navigation    (bool   value) { return(set( CHART_QUICK_NAVIGATION,    value )); }
	CBChart *scale               (int    value) { return(set( CHART_SCALE,               value )); }
	CBChart *scale_fix           (bool   value) { return(set( CHART_SCALEFIX,            value )); }
	CBChart *scale_fix_11        (bool   value) { return(set( CHART_SCALEFIX_11,         value )); }
	CBChart *scale_pt_per_bar    (bool   value) { return(set( CHART_SCALE_PT_PER_BAR,    value )); }
	CBChart *shift               (bool   value) { return(set( CHART_SHIFT,               value )); }
	CBChart *shift_size          (double value) { return(set( CHART_SHIFT_SIZE,          value )); }
	CBChart *show_ask_line       (bool   value) { return(set( CHART_SHOW_ASK_LINE,       value )); }
	CBChart *show_bid_line       (bool   value) { return(set( CHART_SHOW_BID_LINE,       value )); }
	CBChart *show_date_scale     (bool   value) { return(set( CHART_SHOW_DATE_SCALE,     value )); }
	CBChart *show_grid           (bool   value) { return(set( CHART_SHOW_GRID,           value )); }
	CBChart *show_last_line      (bool   value) { return(set( CHART_SHOW_LAST_LINE,      value )); }
	CBChart *show_object_descr   (bool   value) { return(set( CHART_SHOW_OBJECT_DESCR,   value )); }
	CBChart *show_ohlc           (bool   value) { return(set( CHART_SHOW_OHLC,           value )); }
	CBChart *show_one_click      (bool   value) { return(set( CHART_SHOW_ONE_CLICK,      value )); }
	CBChart *show_period_sep     (bool   value) { return(set( CHART_SHOW_PERIOD_SEP,     value )); }
	CBChart *show_price_scale    (bool   value) { return(set( CHART_SHOW_PRICE_SCALE,    value )); }
	CBChart *show_trade_levels   (bool   value) { return(set( CHART_SHOW_TRADE_LEVELS,   value )); }

	ENUM_CHART_MODE         mode()        const { return( (ENUM_CHART_MODE)        get( CHART_MODE         )); }
	ENUM_CHART_VOLUME_MODE  show_volumes() const { return( (ENUM_CHART_VOLUME_MODE) get( CHART_SHOW_VOLUMES )); }
	int    height_in_pixels    (int window = 0) const { return( (int)  get(CHART_HEIGHT_IN_PIXELS,  window)); }
	bool   window_is_visible   (int window = 0) const { return( (bool) get(CHART_WINDOW_IS_VISIBLE, window)); } // r/o
	int    window_y_distance   (int window = 0) const { return( (int)  get(CHART_WINDOW_YDISTANCE,  window)); } // r/o
	bool   autoscroll          () const { return(  (bool) get( CHART_AUTOSCROLL          )); }
	color  color_ask           () const { return( (color) get( CHART_COLOR_ASK           )); }
	color  color_background    () const { return( (color) get( CHART_COLOR_BACKGROUND    )); }
	color  color_bid           () const { return( (color) get( CHART_COLOR_BID           )); }
	color  color_candle_bear   () const { return( (color) get( CHART_COLOR_CANDLE_BEAR   )); }
	color  color_candle_bull   () const { return( (color) get( CHART_COLOR_CANDLE_BULL   )); }
	color  color_chart_down    () const { return( (color) get( CHART_COLOR_CHART_DOWN    )); }
	color  color_chart_line    () const { return( (color) get( CHART_COLOR_CHART_LINE    )); }
	color  color_chart_up      () const { return( (color) get( CHART_COLOR_CHART_UP      )); }
	color  color_foreground    () const { return( (color) get( CHART_COLOR_FOREGROUND    )); }
	color  color_grid          () const { return( (color) get( CHART_COLOR_GRID          )); }
	color  color_last          () const { return( (color) get( CHART_COLOR_LAST          )); }
	color  color_stop_level    () const { return( (color) get( CHART_COLOR_STOP_LEVEL    )); }
	color  color_volume        () const { return( (color) get( CHART_COLOR_VOLUME        )); }
	string comment             () const { return(         get( CHART_COMMENT             )); }
	bool   drag_trade_levels   () const { return( (bool)  get( CHART_DRAG_TRADE_LEVELS   )); }
	bool   foreground          () const { return( (bool)  get( CHART_FOREGROUND          )); }
	bool   event_mouse_move    () const { return( (bool)  get( CHART_EVENT_MOUSE_MOVE    )); }
	bool   event_object_create () const { return( (bool)  get( CHART_EVENT_OBJECT_CREATE )); }
	bool   event_object_delete () const { return( (bool)  get( CHART_EVENT_OBJECT_DELETE )); }
	int    first_visible_bar   () const { return( (int)   get( CHART_FIRST_VISIBLE_BAR   )); } // r/o // Номер первого (левого) видимого бара на графике. Индексация баров соответствует таймсерии.
	double fixed_max           () const { return(         get( CHART_FIXED_MAX           )); }
	double fixed_min           () const { return(         get( CHART_FIXED_MIN           )); }
	double fixed_position      () const { return(         get( CHART_FIXED_POSITION      )); }
	bool   is_object           () const { return( _mql.is4 ? false : ((bool)get(CHART_IS_OBJECT))); } // r/o
	bool   keyboard_control    () const { return( (bool)  get( CHART_KEYBOARD_CONTROL    )); }
	bool   mouse_scroll        () const { return( (bool)  get( CHART_MOUSE_SCROLL        )); }
	double points_per_bar      () const { return(         get( CHART_POINTS_PER_BAR      )); }
	double price_max           (int window = 0) const { return( get( CHART_PRICE_MAX, window )); } // r/o
	double price_min           (int window = 0) const { return( get( CHART_PRICE_MIN, window )); } // r/o
	bool   quick_navigation    () const { return( (bool)  get( CHART_QUICK_NAVIGATION    )); }
	int    scale               () const { return( (int)   get( CHART_SCALE               )); }
	bool   scale_fix           () const { return( (bool)  get( CHART_SCALEFIX            )); }
	bool   scale_fix_11        () const { return( (bool)  get( CHART_SCALEFIX_11         )); }
	bool   scale_pt_per_bar    () const { return( (bool)  get( CHART_SCALE_PT_PER_BAR    )); }
	bool   shift               () const { return( (bool)  get( CHART_SHIFT               )); }
	double shift_size          () const { return(         get( CHART_SHIFT_SIZE          )); } // 10..50%
	bool   show_ask_line       () const { return( (bool)  get( CHART_SHOW_ASK_LINE       )); }
	bool   show_bid_line       () const { return( (bool)  get( CHART_SHOW_BID_LINE       )); }
	bool   show_date_scale     () const { return( (bool)  get( CHART_SHOW_DATE_SCALE     )); }
	bool   show_grid           () const { return( (bool)  get( CHART_SHOW_GRID           )); }
	bool   show_last_line      () const { return( (bool)  get( CHART_SHOW_LAST_LINE      )); }
	bool   show_object_descr   () const { return( (bool)  get( CHART_SHOW_OBJECT_DESCR   )); }
	bool   show_ohlc           () const { return( (bool)  get( CHART_SHOW_OHLC           )); }
	bool   show_one_click      () const { return( (bool)  get( CHART_SHOW_ONE_CLICK      )); }
	bool   show_period_sep     () const { return( (bool)  get( CHART_SHOW_PERIOD_SEP     )); }
	bool   show_price_scale    () const { return( (bool)  get( CHART_SHOW_PRICE_SCALE    )); }
	bool   show_trade_levels   () const { return( (bool)  get( CHART_SHOW_TRADE_LEVELS   )); }
	int    visible_bars        () const { return( (int)   get( CHART_VISIBLE_BARS        )); } // r/o
	int    width_in_bars       () const { return( (int)   get( CHART_WIDTH_IN_BARS       )); } // r/o
	int    width_in_pixels     () const { return( (int)   get( CHART_WIDTH_IN_PIXELS     )); } // r/o
	int    window_handle       () const { return( (int)   get( CHART_WINDOW_HANDLE       )); } // r/o
	int    windows_total       () const { return( (int)   get( CHART_WINDOWS_TOTAL       )); } // r/o

	// имитация через DLL, где необходимо

	bool is_maximized(bool fallback) const
	{
#ifdef __MQL4__
	#ifdef DEBUG
		return(fallback);
	#else
		if (!_mql.dlls_allowed())
			return(fallback);
		
		CBChartWindow w(id_);
		if (!w.is_valid())
			return(fallback);
		
		return(w.is_maximized());
	#endif
#else
		return((bool)get(CHART_IS_MAXIMIZED)); // вероятно, r/o
#endif
	}

	bool is_minimized(bool fallback) const
	{
#ifdef __MQL4__
	#ifdef DEBUG
		return(fallback);
	#else
		if (!_mql.dlls_allowed())
			return(fallback);
		
		CBChartWindow w(id_);
		if (!w.is_valid())
			return(fallback);
		
		return(w.is_minimized());
	#endif
#else
		return((bool)get(CHART_IS_MINIMIZED)); // вероятно, r/o
#endif
	}


	CBChart *maximize()
	{
		if (!_mql.dlls_allowed())
			return(&this);

		CBChartWindow w(id_);
		if (w.is_valid())
			w.maximize();
		
		return(&this);
	}

	CBChart *minimize()
	{
		if (!_mql.dlls_allowed())
			return(&this);
		
		CBChartWindow w(id_);
		if (w.is_valid())
			w.minimize();
		
		return(&this);
	}

	CBChart *restore()
	{
		if (!_mql.dlls_allowed())
			return(&this);
		
		CBChartWindow w(id_);
		if (w.is_valid())
			w.restore();
		
		return(&this);
	}


	// Дополнительные функции

	// Универсальный вариант is_offline
	bool is_custom()
	{
#ifdef __MQL4__
	return((bool)get(CHART_IS_OFFLINE));
#else
	return((bool)::SymbolInfoInteger(symbol(), SYMBOL_CUSTOM));
#endif
	}

	bool is_visible() const
	{
		if (is_minimized(false))
			return(false);
		
		if (is_maximized(true))
			return(true);
		
		// Если есть хотя бы один максимизированный график, кроме текущего, то текущий скрыт за ним
		long this_chart_id = id();
		
		// Переключить все, кроме текущего
		for (CBChart chart; chart.loop_real(); )
		{
			if (chart.id() == this_chart_id)
				continue;
			
			if (chart.is_maximized(false))
				return(false);
		}
		
		/*
		Необходимые функции:
			- окно графика перед текущим?
		*/
		
		return(true);
	}


	// Последний (правый) видимый бар, может быть отрицательный, если справа есть отступ.
	// zero_limit: вернуть 0, если номер бара отрицательный
	int last_visible_bar(bool zero_limit = false) const
	{
		int bar = first_visible_bar() - width_in_bars();
		
		if (zero_limit && (bar < 0))
			bar = 0;
		
		return(bar);
	}

	// синонимы с понятными именами
	int leftmost_visible_bar() const { return(first_visible_bar()); }
	int rightmost_visible_bar(bool zero_limit = false) const { return(last_visible_bar(zero_limit)); }

	// Ширина бара (включая зазор) в пикселях. Или шаг баров в пикселях.
	int bar_width() const
	{
		return(1 << scale());
	}
	
	// Толщина бара как линии (не пиксели).
	int bar_body_line_width()
	{
		int scale = scale();
		
		switch (scale)
		{
			case 0: return(1);
			case 1: return(1);
			case 2: return(2);
			case 3: return(3);
			case 4: return(6);
			case 5: return(12);
		}
		
		_debug.warning("Unknown bar scale:" + VAR(scale));
		return(1);
	}

	// Число баров на графике (не в окне)
	int bars_count() const
	{
		return(::Bars(symbol(), period()));
	}

	double height_in_price() const
	{
		return(this.price_max() - this.price_min());
	}

	// Redraw только для 5 и новее
	CBChart *redraw5()
	{
#ifndef __MQL4__
		::ChartRedraw(id_);
#endif
		return(&this);
	}

	// get chart colors
	BChartColors colors() const
	{
		BChartColors colors;
		
		colors.ask          = color_ask();
		colors.background   = color_background();
		colors.bid          = color_bid();
		colors.candle_bear  = color_candle_bear();
		colors.candle_bull  = color_candle_bull();
		colors.chart_down   = color_chart_down();
		colors.chart_line   = color_chart_line();
		colors.chart_up     = color_chart_up();
		colors.foreground   = color_foreground();
		colors.grid         = color_grid();
		colors.last         = color_last();
		colors.stop_level   = color_stop_level();
		colors.volume       = color_volume();
		
		return(colors);
	}

	// set chart colors
	void colors(const BChartColors &colors)
	{
		color_ask          (colors.ask);
		color_background   (colors.background);
		color_bid          (colors.bid);
		color_candle_bear  (colors.candle_bear);
		color_candle_bull  (colors.candle_bull);
		color_chart_down   (colors.chart_down);
		color_chart_line   (colors.chart_line);
		color_chart_up     (colors.chart_up);
		color_foreground   (colors.foreground);
		color_grid         (colors.grid);
		color_last         (colors.last);
		color_stop_level   (colors.stop_level);
		color_volume       (colors.volume);
	}

	void comment_log(string s)
	{
		// часть экрана, занимаемая логом
		const double screen_part = 0.5;
		
		// число строк. 1 строка занимает 12 пикселей
		int rows = _math.round_to_int((screen_part * height_in_pixels(0)) / 12.0);
		if (rows < 1)
			rows = 1;
		
		string old_comment = comment();
		string lines[];
		int old_count = _str.split(old_comment, "\n", false, false, lines);
		
		int remove_count = old_count - (rows - 1);
		if (remove_count > 0)
			_arr.remove(lines, 0, remove_count);
		
		_arr.add(lines, "[" + ::TimeToString(::TimeLocal(), TIME_MINUTES | TIME_SECONDS) + "] " + s);
		comment(_arr.to_string(lines, "\n"));
	}


protected:

	// Универсальные функции доступа к свойствам
	CBChart *set(ENUM_CHART_PROPERTY_DOUBLE  property_id, double value) { ::ChartSetDouble( id_, property_id, value); return(&this); }
	CBChart *set(ENUM_CHART_PROPERTY_INTEGER property_id, long   value) { ::ChartSetInteger(id_, property_id, value); return(&this); }
	CBChart *set(ENUM_CHART_PROPERTY_STRING  property_id, string value) { ::ChartSetString( id_, property_id, value); return(&this); }
	CBChart *set(ENUM_CHART_PROPERTY_INTEGER property_id, int window, long value) { ::ChartSetInteger(id_, property_id, value); return(&this); }

	double get(ENUM_CHART_PROPERTY_DOUBLE  property_id, int window = 0) const { return(::ChartGetDouble( id_, property_id, window)); }
	long   get(ENUM_CHART_PROPERTY_INTEGER property_id, int window = 0) const { return(::ChartGetInteger(id_, property_id, window)); }
	string get(ENUM_CHART_PROPERTY_STRING  property_id                ) const { return(::ChartGetString( id_, property_id        )); }

	bool get(ENUM_CHART_PROPERTY_DOUBLE  property_id, int window, double &value) const { return(::ChartGetDouble( id_, property_id, window, value)); }
	bool get(ENUM_CHART_PROPERTY_INTEGER property_id, int window, long   &value) const { return(::ChartGetInteger(id_, property_id, window, value)); }
	bool get(ENUM_CHART_PROPERTY_STRING  property_id,             string &value) const { return(::ChartGetString( id_, property_id,         value)); }

} _chart;


#ifdef __MQL4__

class CBChart4: public CBChart
{
public:

	// Конструкторы
	void CBChart4():
		CBChart()
	{
	}

	void CBChart4(long chart_id):
		CBChart(chart_id)
	{
	}

	void CBChart4(string symbol, ENUM_TIMEFRAMES timeframe):
		CBChart(symbol, timeframe)
	{
	}

	// Свойства
	bool is_offline() const { return((bool)get(CHART_IS_OFFLINE)); } // r/o

} _chart4;

#else

class CBChart5: public CBChart
{
public:

	// Конструкторы
	void CBChart5():
		CBChart()
	{
	}

	void CBChart5(long chart_id):
		CBChart(chart_id)
	{
	}

	void CBChart5(string symbol, ENUM_TIMEFRAMES timeframe):
		CBChart(symbol, timeframe)
	{
	}

	// Функции для работы с индикаторами графика, только мт5
	bool   indicator_add    ( int window, int    indicator_handle    ) const { return(::ChartIndicatorAdd    ( id_, window, indicator_handle    )); }
	bool   indicator_delete ( int window, string indicator_shortname ) const { return(::ChartIndicatorDelete ( id_, window, indicator_shortname )); }
	int    indicator_get    ( int window, string indicator_shortname ) const { return(::ChartIndicatorGet    ( id_, window, indicator_shortname )); }
	string indicator_name   ( int window, int    index               ) const { return(::ChartIndicatorName   ( id_, window, index               )); }
	int    indicators_total ( int window                             ) const { return(::ChartIndicatorsTotal ( id_, window                      )); }

	// Свойства
	CBChart *expert_name ( string value ) { return(set( CHART_EXPERT_NAME, value )); }
	CBChart *script_name ( string value ) { return(set( CHART_SCRIPT_NAME, value )); }
	CBChart *show        ( bool   value ) { return(set( CHART_SHOW,        value )); }

	string expert_name () const { return(        get( CHART_EXPERT_NAME )); }
	string script_name () const { return(        get( CHART_SCRIPT_NAME )); }
	bool   show        () const { return( (bool) get( CHART_SHOW        )); }

	// 5.1730?

	CBChart *context_menu        (bool   value) { return(set( CHART_CONTEXT_MENU,        value )); }
	CBChart *crosshair_tool      (bool   value) { return(set( CHART_CROSSHAIR_TOOL,      value )); }
	CBChart *event_mouse_wheel   (bool   value) { return(set( CHART_EVENT_MOUSE_WHEEL,   value )); }

	bool   context_menu        () const { return( (bool)  get( CHART_CONTEXT_MENU        )); }
	bool   crosshair_tool      () const { return( (bool)  get( CHART_CROSSHAIR_TOOL      )); }
	bool   event_mouse_wheel   () const { return( (bool)  get( CHART_EVENT_MOUSE_WHEEL   )); }


	// 5.1910

	bool   is_docked    () const { return( (bool)  get( CHART_IS_DOCKED    )); }  // окно графика закреплено. Если установить значение false, то график можно перетащить  за пределы терминала
	int    float_left   () const { return( (int)   get( CHART_FLOAT_LEFT   )); }  // левая координата открепленного графика относительно виртуального экрана
	int    float_top    () const { return( (int)   get( CHART_FLOAT_TOP    )); }  // верхняя координата открепленного графика относительно виртуального экрана
	int    float_right  () const { return( (int)   get( CHART_FLOAT_RIGHT  )); }  // правая координата открепленного графика  относительно виртуального экрана
	int    float_bottom () const { return( (int)   get( CHART_FLOAT_BOTTOM )); }  // нижняя координата открепленного графика  относительно виртуального экрана

	CBChart *is_docked    (bool value) { return(set( CHART_IS_DOCKED,    value )); }
	CBChart *float_left   (int  value) { return(set( CHART_FLOAT_LEFT,   value )); }
	CBChart *float_top    (int  value) { return(set( CHART_FLOAT_TOP,    value )); }
	CBChart *float_right  (int  value) { return(set( CHART_FLOAT_RIGHT,  value )); }
	CBChart *float_bottom (int  value) { return(set( CHART_FLOAT_BOTTOM, value )); }

} _chart5;

#endif

