/*
Copyright 2019 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Класс доступа к свойствам исторических данных, 5.1730/4.1601. Better Standard Library. © FXcoder

#property strict

#include "type/array/arrayptrt.mqh"
#include "symbolseries.mqh"


// шаблон для функций копирования истории, их слишком много (32-36), чтобы поддерживать все по отдельности
#define SERIES_COPY_TPL(FUNC, SFUNC, T)                                              \
	bool FUNC(int pos, T &value) const                                               \
	{                                                                                \
		T arr[];                                                                     \
		if (::SFUNC(symbol_, period_, pos, 1, arr) != 1)                             \
			return(false);                                                           \
		value = arr[0];                                                              \
		return(true);                                                                \
	}                                                                                \
	int FUNC(     int start_pos,       int count,     T &arr[]) const { return(::SFUNC(symbol_, period_, start_pos,  count,     arr)); }  \
	int FUNC(datetime start_time,      int count,     T &arr[]) const { return(::SFUNC(symbol_, period_, start_time, count,     arr)); }  \
	int FUNC(datetime start_time, datetime stop_time, T &arr[]) const { return(::SFUNC(symbol_, period_, start_time, stop_time, arr)); }


// шаблон для функций получения котировок с типом float
#define SERIES_COPY_FLOAT_TPL(FUNC, SFUNC)                                           \
	bool FUNC##_float(int pos, float &value) const                                   \
	{                                                                                \
		double arr_d[];                                                              \
		if (::SFUNC(symbol_, period_, pos, 1, arr_d) != 1)                           \
			return(false);                                                           \
		value = float(arr_d[0]);                                                     \
		return(true);                                                                \
	}                                                                                \
	int FUNC##_float(int start_pos, int count, float &arr[]) const {                 \
		double arr_d[];                                                              \
		int res = ::SFUNC(symbol_, period_, start_pos, count, arr_d);                \
		if (res < 1)                                                                 \
			return(res);                                                             \
		::ArrayCopy(arr, arr_d);                                                     \
		return(res);                                                                 \
	}                                                                                \
	int FUNC##_float(datetime start_time, int count, float &arr[]) const {           \
		double arr_d[];                                                              \
		int res = ::SFUNC(symbol_, period_, start_time, count, arr_d);               \
		if (res < 1)                                                                 \
			return(res);                                                             \
		::ArrayCopy(arr, arr_d);                                                     \
		return(res);                                                                 \
	}                                                                                \
	int FUNC##_float(datetime start_time, datetime stop_time, float &arr[]) const {  \
		double arr_d[];                                                              \
		int res = ::SFUNC(symbol_, period_, start_time, stop_time, arr_d);           \
		if (res < 1)                                                                 \
			return(res);                                                             \
		::ArrayCopy(arr, arr_d);                                                     \
		return(res);                                                                 \
	}


class CBSymbolSeries;

class CBSeries
{
protected:

	const string symbol_;
	const ENUM_TIMEFRAMES period_;
	const int period_seconds_;
	CBArrayPtrT<CBSymbolSeries> symbol_series_pool_;


public:

	// Для двойной индексации (_series["EURUSD"][PERIOD_D1])

	CBSymbolSeries *operator [](string symbol)
	{
		for (int i = symbol_series_pool_.size() - 1; i >= 0; i--)
		{
			if (symbol_series_pool_.data[i].symbol() == symbol)
				return(symbol_series_pool_.data[i]);
		}

		return(symbol_series_pool_.add_return(new CBSymbolSeries(symbol)));
	}


	// Основная часть

	void CBSeries():
		symbol_(_Symbol),
		period_(PERIOD_CURRENT),
		period_seconds_(::PeriodSeconds(PERIOD_CURRENT))
	{
	}

	void CBSeries(string symbol, ENUM_TIMEFRAMES period):
		symbol_(symbol),
		period_(period),
		period_seconds_(::PeriodSeconds(period))
	{
	}

	//void ~CBSeries() { symbol_series_pool_.safe_delete(); }

	string          symbol() const { return(symbol_); }
	ENUM_TIMEFRAMES period() const { return(period_); }


	// свойства int/bool
	int      bars_count        () { return( (int)      info(SERIES_BARS_COUNT)         ); } // long в справке по ошибке? остальные баровые функции работают с int
	datetime first_date        () { return( (datetime) info(SERIES_FIRSTDATE)          ); }
	datetime last_bar_date     () { return( (datetime) info(SERIES_LASTBAR_DATE)       ); }
	datetime server_first_date () { return( (datetime) info(SERIES_SERVER_FIRSTDATE)   ); }

	int bars() const
	{
		return(::Bars(symbol_, period_));
	}

	int bars(datetime start_time, datetime stop_time) const
	{
		return(::Bars(symbol_, period_, start_time, stop_time));
	}

	SERIES_COPY_TPL(copy_rates,       CopyRates,      MqlRates)
	SERIES_COPY_TPL(copy_time,        CopyTime,       datetime)
	SERIES_COPY_TPL(copy_open,        CopyOpen,       double)
	SERIES_COPY_TPL(copy_high,        CopyHigh,       double)
	SERIES_COPY_TPL(copy_low,         CopyLow,        double)
	SERIES_COPY_TPL(copy_close,       CopyClose,      double)
	SERIES_COPY_TPL(copy_tick_volume, CopyTickVolume, long)
	SERIES_COPY_TPL(copy_spread,      CopySpread,     int)

	SERIES_COPY_FLOAT_TPL(copy_open,  CopyOpen)
	SERIES_COPY_FLOAT_TPL(copy_high,  CopyHigh)
	SERIES_COPY_FLOAT_TPL(copy_low,   CopyLow)
	SERIES_COPY_FLOAT_TPL(copy_close, CopyClose)


	// Обёртки для стандартных функций типа iTime, iBarShift.

	int bar_shift(datetime time, bool exact = false) const
	{
		int bar = ::iBarShift(symbol_, period_, time, exact);
		
		// mki#11: в 5.1881 iBarShift возвращает -1, если бар за пределами истории (в 4 возвращает последний бар)
		if (exact || (bar >= 0))
			return(bar);
		
		return(bars() - 1);
	}

	datetime time   (int index) const { return(::iTime  (symbol_, period_, index)); }
	double   low    (int index) const { return(::iLow   (symbol_, period_, index)); }
	double   high   (int index) const { return(::iHigh  (symbol_, period_, index)); }
	double   close  (int index) const { return(::iClose (symbol_, period_, index)); }
	double   open   (int index) const { return(::iOpen  (symbol_, period_, index)); }
	long     volume (int index) const { return(::iVolume(symbol_, period_, index)); }

#ifdef __MQL4__
	int    highest      (int type, int count = WHOLE_ARRAY, int start = 0) const { return(::iHighest(symbol_, period_, type, count, start)); }
	int    lowest       (int type, int count = WHOLE_ARRAY, int start = 0) const { return(::iLowest (symbol_, period_, type, count, start)); }

	double highest_value(int type, int count = WHOLE_ARRAY, int start = 0) const { return(high(highest(type, count, start))); }
	double lowest_value (int type, int count = WHOLE_ARRAY, int start = 0) const { return(low (lowest (type, count, start))); }

#else
	int    highest      (ENUM_SERIESMODE type, int count = WHOLE_ARRAY, int start = 0) const { return(::iHighest(symbol_, period_, type, count, start)); }
	int    lowest       (ENUM_SERIESMODE type, int count = WHOLE_ARRAY, int start = 0) const { return(::iLowest (symbol_, period_, type, count, start)); }

	double highest_value(ENUM_SERIESMODE type, int count = WHOLE_ARRAY, int start = 0) const { return(high(highest(type, count, start))); }
	double lowest_value (ENUM_SERIESMODE type, int count = WHOLE_ARRAY, int start = 0) const { return(low (lowest (type, count, start))); }
#endif

	// дополнительные функции

	/**
	Получить время указанного бара и таймфрейма.
	@param bar         Номер искомого бара.
	@param zero_limit  Не допускать отрицательных значений баров.
	@return            Время искомого бара. Если указан бар с отрицательным номером, возвращается время из будущего без учета
	                   выходных (терминал использует этот же способ для рисования сетки периодов).
	*/
	datetime bar_time(int bar)//, bool zero_limit = false)
	{
		bool zero_limit = false;
		
		if (bar >= 0)
			return(time(bar));
		
		if (zero_limit)
			return(time(0));
		
		return(time(0) - bar * period_seconds_);
	}

	/**
	Получить номер бара для указанного времени и таймфрейма.
	@param time        Время искомого бара.
	@param zero_limit  Не допускать отрицательных значений. При true будет работать как bar_shift(int,false).
	@return            Номер бара. Если бар выходит за границу котировок справа, возвращается соответствующее отрицательные
	                   значение.
	*/
	int time_bar(datetime time)//, bool zero_limit = false)
	{
		bool zero_limit = false;
		
		int bar = bar_shift(time);
		datetime t = time(bar);
		
		// на случай времени за последним баром
		if (!zero_limit && (bar == 0) && (t != time))
			bar = (int)((time(0) - time) / period_seconds_);
		
		return(bar);
	}

	/**
	Получить номер бара для указанного времени и таймфрейма со смещением вправо.
	@param time        Время искомого бара.
	@return            Номер бара. Если бар выходит за границу котировок справа, возвращается соответствующее отрицательное
	                   значение. Если найденный бар имеет время раньше указанного, то возвращается бар справа.
	*/
	int time_bar_right(datetime time)
	{
		int bar = bar_shift(time);
		datetime t = time(bar);

		if ((t != time) && (bar == 0))
		{
			// время за пределами диапазона
			bar = (int)((time(0) - time) / period_seconds_);
		}
		else
		{
			// проверить, чтобы бар был не слева по времени
			if (t < time)
				bar--;
		}
		
		return(bar);
	}

	/**
	Получить номер бара для указанного времени и таймфрейма со смещением влево.
	@param time    Время искомого бара.
	@return        Номер бара. Если бар выходит за границу котировок справа, возвращается соответствующее отрицательное
	               значение. Если найденный бар имеет время раньше указанного, то возвращается бар слева.
	*/
	int time_bar_left(datetime time)
	{
		int bar = bar_shift(time);
		datetime t = time(bar);

		if ((t != time) && (bar == 0))
		{
			// время за пределами диапазона
			bar = (int)((time(0) - time) / period_seconds_);
		}
		else
		{
			// проверить, чтобы бар был не справа по времени
			if (t > time)
				bar++;
		}
		
		return(bar);
	}


protected:

	// Универсальные функции доступа к свойствам
	long info(const ENUM_SERIES_INFO_INTEGER property_id             ) { return(::SeriesInfoInteger(symbol_, period_, property_id)); }
	bool info(const ENUM_SERIES_INFO_INTEGER property_id, long &value) { return(::SeriesInfoInteger(symbol_, period_, property_id, value)); }

} _series;


#ifndef __MQL4__

class CBSeries5: public CBSeries
{
public:

	void CBSeries5():                                      CBSeries()               { }
	void CBSeries5(string symbol, ENUM_TIMEFRAMES period): CBSeries(symbol, period) { }

	// Свойства
	datetime terminal_first_date () { return( (datetime) info(SERIES_TERMINAL_FIRSTDATE) ); }
	bool     synchronized        () { return( (bool)     info(SERIES_SYNCHRONIZED)       ); }

	SERIES_COPY_TPL(copy_real_volume, CopyRealVolume, long)

} _series5;

#endif

