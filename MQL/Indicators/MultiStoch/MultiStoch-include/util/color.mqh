/*
Copyright 2019 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Цветовые функции. © FXcoder

#property strict

#include "../bsl.mqh"


void get_random_two_colors(const int seed, const int iterations, const color counter_color, color &color1, color &color2)
{
	if (seed != 0)
		MathSrand(seed);

	double best_score = 0;

	int rc, gc, bc;
	_color.to_rgb(counter_color, rc, gc, bc);
	double h0 = _math.stdev(rc, gc, bc);

	for (int i = 0; i < iterations; i++)
	{
		color c1 = _color.random();
		color c2 = _color.random();

		int r1, g1, b1;
		int r2, g2, b2;
		
		_color.to_rgb(c1, r1, g1, b1);
		_color.to_rgb(c2, r2, g2, b2);
		
		//int score = fabs(r - b) + fabs(b - g) + fabs(g - r);

		// между разными компонентами цвета
		double h1 = _math.stdev(r1, g1, b1);
		double h2 = _math.stdev(r2, g2, b2);
		
		// между одними компонтами разных цветов
		double v1 = _math.stdev(r1, r2, rc);
		double v2 = _math.stdev(g1, g2, rc);

		// между каждым цветом и контр-цветом
		double cv1 = _math.stdev(r1, rc) + _math.stdev(g1, gc) + _math.stdev(b1, bc);
		double cv2 = _math.stdev(r2, rc) + _math.stdev(g2, gc) + _math.stdev(b2, bc);
		
		double score = (h0 + h1 + h2) * (v1 + v2) * (cv1 + cv2);
		
		if (score > best_score)
		{
			best_score = score;
			
			color1 = c1;
			color2 = c2;
		}
	}
}

// counter_color - контр-цвет, от которого результирующие цвета тоже должны отличаться
void get_random_three_colors(const int seed, const int iterations, const color counter_color, color &color1, color &color2, color &color3)
{
	if (seed != 0)
		MathSrand(seed);

	double best_score = 0;

	int rc, gc, bc;
	_color.to_rgb(counter_color, rc, gc, bc);
	double h0 = _math.stdev(rc, gc, bc);

	for (int i = 0; i < iterations; i++)
	{
		color c1 = _color.random();
		color c2 = _color.random();
		color c3 = _color.random();

		int r1, g1, b1;
		int r2, g2, b2;
		int r3, g3, b3;
		
		_color.to_rgb(c1, r1, g1, b1);
		_color.to_rgb(c2, r2, g2, b2);
		_color.to_rgb(c3, r3, g3, b3);
		
		// между разными компонентами цвета
		double h1 = _math.stdev(r1, g1, b1);
		double h2 = _math.stdev(r2, g2, b2);
		double h3 = _math.stdev(r3, g3, b3);
		
		// между одними компонтами разных цветов
		double v1 = _math.stdev(r1, r2, r3, rc);
		double v2 = _math.stdev(g1, g2, g3, gc);
		double v3 = _math.stdev(b1, b2, b3, bc);
		
		// между каждым цветом и контр-цветом
		double cv1 = _math.stdev(r1, rc) + _math.stdev(g1, gc) + _math.stdev(b1, bc);
		double cv2 = _math.stdev(r2, rc) + _math.stdev(g2, gc) + _math.stdev(b2, bc);
		double cv3 = _math.stdev(r3, rc) + _math.stdev(g3, gc) + _math.stdev(b3, bc);
		
		double score = (h0 + h1 + h2 + h3) * (v1 + v2 + v3) * (cv1 + cv2 + cv3);
		
		if (score > best_score)
		{
			best_score = score;
			
			color1 = c1;
			color2 = c2;
			color3 = c3;
		}
	}
}
