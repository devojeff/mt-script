/*
Copyright 2019 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Color constants. � FXcoder

#property strict

// Additional colors (add new alphabetically)
#define clrBrandeisBlue      C'0,112,255'
#define clrCerise            C'222,49,99'
#define clrChineseBlack      C'20,20,20'
#define clrChristmasPurple   C'77,8,75'
#define clrDeepCarminePink   C'239,48,56'
#define clrPineGreen         C'1,121,111'
#define clrRichBlack         C'0,64,64'
#define clrSchoolBusYellow   C'255,216,0'

// Deal colors
#define DEAL_COLOR_BUY       clrDodgerBlue // C'3,95,172'
#define DEAL_COLOR_SELL      clrOrangeRed  // C'225,68,29'
