/*
Copyright 2019 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Класс набора простых линий графика с градиентной расцветкой (для индикаторов серии Multi). © FXcoder

#property strict

#include "../bsl.mqh"
#include "../enum/gradient.mqh"
#include "../enum/multi_theme.mqh"
#include "../util/theme.mqh"
#include "lineplotset.mqh"

class CGradientLinePlotSet: public CLinePlotSet
{
protected:

	color colors_[];
	bool is_initialized_;


public:

	void CGradientLinePlotSet():
		CLinePlotSet()
	{
		is_initialized_ = false;
		::ArrayFree(colors_);
	}

	void CGradientLinePlotSet(
		int plot_index_first,
		int buffer_index_first,
		int count,
		ENUM_MULTI_THEME theme,
		uint seed,
		color start_color,
		color mid_color,
		color end_color,
		ENUM_GRADIENT gradient_type,
		bool reverse):
			CLinePlotSet(),
			is_initialized_(false)
	{
		init(plot_index_first, buffer_index_first, count, theme, seed, start_color, mid_color, end_color, gradient_type, reverse);
	}

	bool is_initialized() { return(is_initialized_); }

	CGradientLinePlotSet *init(
		int plot_index_first,
		int buffer_index_first,
		int count,
		ENUM_MULTI_THEME theme,
		uint seed,
		color start_color,
		color mid_color,
		color end_color,
		ENUM_GRADIENT gradient_type,
		bool reverse)
	{
		CLinePlotSet::init(plot_index_first, buffer_index_first, count);
		
		bool is_coloring_enabled = !(_color.is_none(start_color) || _color.is_none(end_color));
				
		if (is_coloring_enabled)
		{
			if (!color_gradient(theme, seed, start_color, mid_color, end_color, count, gradient_type, colors_))
			{
				is_initialized_ = false;
				return(&this);
			}
			
			if (reverse)
				_arr.reverse(colors_);
			
			for (int p = 0; p < count_; p++)
				plots_[p].line_color(colors_[p]);
		}
		
		is_initialized_ = true;
		return(&this);
	}

	int get_colors(color &colors[])
	{
		_arr.clone(colors, colors_);
		return(::ArraySize(colors));
	}

};
