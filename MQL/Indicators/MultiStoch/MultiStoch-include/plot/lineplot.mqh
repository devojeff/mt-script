/*
Copyright 2019 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Класс индикарной линии: Линия (DRAW_LINE). © FXcoder

#property strict

#include "singleplot.mqh"


class CLinePlot: public CSinglePlot
{
public:

	void CLinePlot(): CSinglePlot() { }

	void CLinePlot(int plot_index_first, int buffer_index_first): CSinglePlot()
	{
		init(plot_index_first, buffer_index_first);
	}

	void CLinePlot(const CPlot &prev): CSinglePlot()
	{
		init(prev);
	}

	virtual CLinePlot *init(int plot_index_first, int buffer_index_first) override
	{
		init_single(plot_index_first, buffer_index_first, DRAW_LINE);
		return(&this);
	}

	virtual CLinePlot *init(const CPlot &prev) override
	{
		init_single(prev.plot_index_next(), prev.buffer_index_next(), DRAW_LINE);
		return(&this);
	}

};

